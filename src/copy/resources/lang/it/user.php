<?php

return [
    'management' => 'Gestione Utenti',

    'users' => 'Utenti',
    'user' => 'Utente',

    'id' => '#',
    'name' => 'Nome',
    'email' => 'E-Mail',
    'password' => 'Password',
    'profile_image' => 'Foto Profilo',
    'roles' => 'Ruoli'
];