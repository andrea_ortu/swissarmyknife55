<?php

namespace Mmrp\Swissarmyknife\Lib\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Mmrp\Swissarmyknife\Lib\SpoutTrait;
use Symfony\Component\HttpFoundation\File\File;

class ExportData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, SpoutTrait;

    protected $toJob;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($toJob)
    {
        $this->toJob = $toJob;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $filename = $this->toJob->filename;
        $delimiter = $this->toJob->delimiter;

        $model = $this->filterAndOrder($this->toJob->input, $this->toJob->model);
        $model = $model->get()->toArray();

        $this->createExcelExport($model , $filename, $delimiter);

        if(env('UPLOAD_TO_S3') == 'true') {
            $this->moveToBucketS3($filename);
        }

        Mail::to($this->toJob->to)->send(new \Mmrp\Swissarmyknife\Mail\ExportData($this->toJob->subject, $filename, $this->toJob->download_link));

    }

    public function failed(\Exception $exception)
    {
        //
    }

    private function createExcelExport($model, $filename, $delimiter = NULL)
    {
        if(!is_dir(env('FILE_PATH') . 'export/data/')){
            mkdir(env('FILE_PATH') . 'export/data/',0777,TRUE);
        }

        $this->laravelCollectionToSpreadsheetFile($model,env('FILE_PATH') . 'export/data/' . $filename ,'xlsx', $delimiter);
    }

    private function moveToBucketS3($filename)
    {
        $file = new File(env('FILE_PATH') . 'export/data/' . $filename);
        Storage::disk('s3')->put('/export/data/' . $file->getFilename(), file_get_contents($file));

        unlink(env('FILE_PATH') . 'export/data/' . $filename);
    }

    private function filterAndOrder($input,$resource)
    {
        if (count($input)) {
            foreach ($input as $filter => $value) {

                if ($value and preg_match('/^s_/',$filter)) {
                    $resource = $resource->where(substr($filter,2), 'like', '%' . str_replace(' ', '%', addslashes($value)) . '%');
                }

                if ($value and preg_match('/^o_/',$filter)) {
                    $resource = $resource->orderBy(substr($filter,2), $value );
                }
            }
        }

        return $resource;
    }
}
