<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 13/06/17
 * Time: 12:05
 */

namespace Mmrp\Swissarmyknife\Controller\Traits;

use Illuminate\Http\Request;
use Mmrp\Swissarmyknife\Lib\Log;

trait TrashTrait
{
    /**
     * Used to enable/disable trash() method
     * @var bool
     */
    protected $trash = TRUE;

    /**
     * Return Trash View
     * @param Request $request
     * @return View
     */
    public function trash(Request $request)
    {
        if(!$this->trash){
            abort(501);
        }

        try {
            if(!is_null($this->related)) {
                $this->model = $this->model->with($this->related);
            }

            $this->model = $this->filterAndOrder($request, $this->model);
            $this->model = $this->addFilter($request,$this->model);
            $this->model = $this->model->orderBy('id')->onlyTrashed();

            if($request->input('per_page') == 'all'){
                $this->model = $this->model->get();
            } else {
                $this->model = $this->model->paginate(($request->input('per_page')) ? $request->input('per_page') : 25);
                $this->model->appends($request->input())->links();
            }

            $this->prepareTrash($request);

            Log::info(new \Exception('trash', 200), $request,
                [
                    'action' => 'trash',
                    'resource' => $this->resource,
                ]
            );

            return $this->response($request, [
                'data' => $this->model,
                'additional_data' =>$this->additional_data,
                'ui' => [
                    'trash' => TRUE,
                    'action' => $this->action,
                    'parameters' => $this->parameters,
                    'resource' => $this->resource,
                    'title' => $this->title,
                    'fields' => $this->fields,
                    'translate_fields' => $this->translate_fields,
                    'fields_types' => $this->fields_types,
                    'breadcrumbs' =>$this->breadcrumbs,
                    'available_action' => $this->prepareAction(),
                    'custom_actions' => $this->custom_action,
                ]
            ]);
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'trash',
                    'resource' => $this->resource,
                ]
            );
        }
    }

    /**
     * Called by child class, prepare @trash environment
     * @param Request $request
     */
    protected function prepareTrash(Request $request)
    {

    }

}