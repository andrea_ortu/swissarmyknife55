<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 13/06/17
 * Time: 12:02
 */

namespace Mmrp\Swissarmyknife\Controller\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Mmrp\Swissarmyknife\Lib\Log;

trait DeleteTrait
{
    /**
     * Used to enable/disable delete() method
     * @var bool
     */
    protected $delete = TRUE;

    /**
     * Delete the specified line
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, $id)
    {
        if(!$this->delete){
            abort(501);
        }

        $id = $request->route()->parameter('id');

        if($id == 'multiple' and empty($request->input('rows_id'))){
            return [
                'code' => 400,
                'message' => 'no line selected'
            ];
        }

        try{
            $this->beforeDelete($request, $id);
            if($id == 'multiple'){
                $this->model = $this->model->whereIn('id',$request->input('rows_id'));
            } else {
                $this->model = $this->model->where('id',$id)->first();
            }

            if(!is_null($this->model)){
                $this->model->delete();
            }

            $this->afterDelete($request, $id);

            if(!is_null($this->redirect_to)){
                $redirect = $this->redirect_to;
            } else {
                $redirect = action($this->action . '@index', $this->parameters);
            }

            return [
                'status' => trans('messages.edit.deleted'),
                'id' => $id,
                'redirect_to' => $redirect
            ];
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'delete',
                    'resource' => $this->resource,
                    'resource_id' => $id
                ]
            );
        }
    }

    /**
     * Called by child class, execute your code before $this->model->delete()
     * @param Request $request
     * @param $id
     */
    protected function beforeDelete(Request $request, $id)
    {

    }

    /**
     * Called by child class, executed after $this->model->delete()
     * @param Request $request
     * @param $id
     */
    protected function afterDelete(Request $request, $id)
    {

    }
}