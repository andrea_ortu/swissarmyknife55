<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 13/06/17
 * Time: 11:57
 */

namespace Mmrp\Swissarmyknife\Controller\Traits;

use Illuminate\Http\Request;
use Mmrp\Swissarmyknife\Lib\Log;

trait IndexTrait
{
    /**
     * Used to enable/disable index() method
     * @var bool
     */
    protected $index = TRUE;

    /**
     * Return Index View
     * @param Request $request
     * @return View
     */
    public function index(Request $request)
    {
        if(!$this->index){
            abort(501);
        }

        try {
            if(!is_null($this->related)) {
                $this->model = $this->model->with($this->related);
            }

            $this->model = $this->filterAndOrder($request, $this->model);
            $this->model = $this->addFilter($request,$this->model);

            if($request->input('per_page') == 'all'){
                $this->model = $this->model->get();
            } else {
                $this->model = $this->model->paginate($request->input('per_page', 25));
                $this->model->appends($request->input())->links();
            }
            $this->prepareIndex($request);

            Log::info(new \Exception('getIndex', 200), $request,
                [
                    'action' => 'getIndex',
                    'resource' => $this->resource,
                ]
            );

            return $this->response($request, [
                'data' => $this->model,
                'additional_data' =>$this->additional_data,
                'batch_import' => method_exists($this,'initBatchImportTrait'),
                'ui' => [
                    'action' => $this->action,
                    'parameters' => $this->parameters,
                    'resource' => $this->resource,
                    'title' => $this->title,
                    'fields' => $this->fields,
                    'translate_fields' => $this->translate_fields,
                    'fields_types' => $this->fields_types,
                    'breadcrumbs' =>$this->breadcrumbs,
                    'available_action' => $this->prepareAction(),
                    'custom_actions' => $this->custom_action,
                ]
            ]);
        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'getIndex',
                    'resource' => $this->resource,
                ]
            );
        }
    }

    /**
     * Called by child class, prepare @index environment
     * @param Request $request
     */
    protected function prepareIndex(Request $request)
    {

    }
}