<?php
/**
 * Created by PhpStorm.
 * User: matteo
 * Date: 03/11/17
 * Time: 10.59
 */

namespace Mmrp\Swissarmyknife\Controller\Traits;

use Illuminate\Http\Request;


trait Response
{
    protected function response(Request $request, $data)
    {
        if(preg_match('/api/',$request->route()->getPrefix())) {
            //return json
            return $data;
        } else {
            //return view
            dd('return view');
        }
    }
}