<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 13/06/17
 * Time: 11:43
 */

namespace Mmrp\Swissarmyknife\Controller\Traits;

use Illuminate\Http\Request;
use Mmrp\Swissarmyknife\Lib\Log;

trait SearchTrait
{
    /**
     * Used to enable/disable search() method
     * @var bool
     */
    protected $search = FALSE;

    /**
     * Return Search View
     * @param Request $request
     * @return View
     */
    public function search(Request $request)
    {
        if(!$this->search){
            abort(501);
        }

        $this->insert = FALSE;
        $this->edit = FALSE;
        $this->trash = FALSE;
        $this->delete = FALSE;
        $this->destroy = FALSE;

        $needle = $request->input('q');

        try {
            if(!is_null($this->related)) {
                $this->model = $this->model->with($this->related);
            }

            foreach ($this->fields as $field){
                $this->model = $this->model->orWhere($field,'like','%' . $needle .'%');
            }

            $this->model = $this->addFilter($request,$this->model);
            $this->model = $this->model->paginate(($request->input('per_page')) ? $request->input('per_page') : 25);

            $this->prepareIndex($request);

            Log::info(new \Exception('getIndex', 200), $request,
                [
                    'action' => 'getIndex',
                    'resource' => $this->resource,
                ]
            );

            return $this->response($request, [
                'data' => $this->model,
                'additional_data' =>$this->additional_data,
                'batch_import' => method_exists($this,'initBatchImportTrait'),
                'ui' => [
                    'action' => $this->action,
                    'parameters' => $this->parameters,
                    'resource' => $this->resource,
                    'title' => $this->title,
                    'fields' => $this->fields,
                    'translate_fields' => $this->translate_fields,
                    'fields_types' => $this->fields_types,
                    'breadcrumbs' =>$this->breadcrumbs,
                    'available_action' => $this->prepareAction(),
                    'custom_actions' => $this->custom_action,
                ]
            ]);

        }
        catch (\Exception $e) {
            Log::info($e, $request, [
                    'action' => 'getIndex',
                    'resource' => $this->resource,
                ]
            );
        }
    }




}