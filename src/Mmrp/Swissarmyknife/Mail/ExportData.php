<?php

namespace Mmrp\Swissarmyknife\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExportData extends Mailable
{
    use Queueable, SerializesModels;

    public $subject = NULL;

    protected $filename = NULL;
    protected $link = NULL;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $filename, $link)
    {
        $this->subject = $subject;

        $this->filename = $filename;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.export_data')->with('link', $this->link );
    }
}
